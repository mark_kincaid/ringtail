import discord
from discord.ext import commands
import os, asyncio
import logging
logger = logging.getLogger(__name__)


#import all of the cogs
from music_cog import music_cog

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='>', intents=intents)

#remove the default help command so that we can write out own
bot.remove_command('help')

@bot.event
async def on_ready():
    logger.info(f'Logged in as {bot.user} (ID: {bot.user.id})')


async def main():
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
    async with bot:
        await bot.add_cog(music_cog(bot))
        await bot.start(os.getenv('TOKEN'))
        logger.info("Ringtail started successfully")

asyncio.run(main())
