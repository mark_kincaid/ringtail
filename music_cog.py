from ast import alias
import discord
from discord.ext import commands
import yt_dlp
import logging
logger = logging.getLogger(__name__)

intents = discord.Intents.default()
intents.message_content = True
intents.voice_states = True

FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
YDL_OPTIONS ={
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,

    'default_search': 'auto',
    'source_address': '0.0.0.0',  # bind to ipv4 since ipv6 addresses cause issues sometimes
}
class music_cog(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.queue = []

    async def beatRequestsOnly(self, ctx):
        if(not str(ctx.message.channel) == 'beat-requests'):
            user = self.client.get_user(ctx.message.author.id)
            with open('img/beatRequestsOnly.jpg', 'rb') as fp:
                await user.send(file=discord.File(fp, 'img/beatRequestsOnly.jpg'))
            await ctx.message.delete()
            await ctx.author.move_to(None)
            logger.info(ctx.author+" kicked for not putting song request in beatrequest channel")
            return False
        return True

    @commands.command(name="play", aliases=["p","playing"], help="Plays a selected song from youtube")
    async def play(self, ctx, *, search):
        try:
                correctChannel = await self.beatRequestsOnly(ctx)
                if(correctChannel):
                    async with ctx.typing():
                        logger.info("Started song download")
                        with yt_dlp.YoutubeDL(YDL_OPTIONS) as ydl:
                            info = ydl.extract_info(f"ytsearch:{search}", download=False)
                            if 'entries' in info:                                   
                                info = info['entries'][0]
                                url = info['url']
                                title = info['title']
                            self.queue.append((url, title))
                            await ctx.send(f'Added to queue : **{title}**')
                            logger.info("song downloaded")
                    if not ctx.voice_client.is_playing():
                        await self.play_next(ctx)
        except Exception as e:
            logger.error("error downloading song: "+e)

    async def after_play(self,ctx,e):
        if e:
            logger.error("error calling after play: " + e )
        else:
            self.client.loop.create_task(self.play_next(ctx))
            
    async def play_next(self, ctx):
        try:
            if self.queue:
                url, title = self.queue.pop(0)
                logger.info(f'Now playing: **{title}**')
                ctx.voice_client.play(discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(url, **FFMPEG_OPTIONS)),after= after_play(self,ctx,e))
                await ctx.send(f'Now playing: **{title}**')
            elif not ctx.voice_client.is_playing():
                logger.info("queue is empty")
                await ctx.send("Queue is empty!")
                if ctx.voice_client:
                    await ctx.voice_client.disconnect()
        except Exception as e:
            logger.error("error streaming song: "+ e )

    @commands.command()
    async def skip(self, ctx):
        if ctx.voice_client and ctx.voice_client.is_playing():
            ctx.voice_client.stop()
            logger.info("song skipped")
            await ctx.send("Skipped ⏭")

    @commands.command()
    async def stop(self, ctx):
        if ctx.voice_client:
             ctx.voice_client.stop()
             self.queue.clear()
             logger.info("song stopped")
             await ctx.voice_client.disconnect()

    @play.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
                logger.info("bot joined the channel")
            else:
                logger.error("user tried to play song whilst not in a voice channel")
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")
  
